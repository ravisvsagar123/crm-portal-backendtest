const Poll = require("pg").Pool;
const pool = new Poll({

    /*
        user: 'bullforce',
        password: '1234',
        host: 'localhost',
        port: '5432',
        database: 'bullforce_crm'
    */
    user: process.env.POSTGRES_DATA_SOURCE_USERNAME,
    host: 'localhost',
    database: 'bullforce_crm',
    password: process.env.POSTGRES_DATA_SOURCE_PASSWORD,
    port: '5432',

});

module.exports = pool;