const express = require("express");

const app = express();
const cors = require("cors");
const pool = require("./dbconfig");



app.use(cors());
// parse requests of content-type - application/json
app.use(express.json());


// parse requests of content-type - application/x-www-form-urlencoded


// simple route

app.post("/demo", async (req, res) => {
    try {


        const { emailid } = req.body;
        const { fname } = req.body;
        const { lname } = req.body;
        const { mobno } = req.body;
        const { address } = req.body;
        const { cname } = req.body;
        const { cuname } = req.body;
        const { acctye } = req.body;
        const { clname } = req.body;
        const { password1 } = req.body;
        const { dob } = req.body;
        const { mobilecuntrtcode } = req.body;
        var htype = "D";
        const insertdemo = await pool.query("insert into accountholders(email,fname,lname,mobileno,address,country,currency_choice,account_type,client_type,password,dob,holdertype,countrycode) values($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13)", [emailid, fname, lname, mobno, address, cname, cuname, acctye, clname, password1, dob, htype, mobilecuntrtcode]);
        res.json(insertdemo.status);
    } catch (err) {
        console.log(err.message);
        res.json(err.message);
    }
});

app.post("/real", async (req, res) => {
    try {


        const { email } = req.body;
        const { fname } = req.body;
        const { lname } = req.body;
        const { mobno } = req.body;
        const { address } = req.body;
        const { countryname } = req.body;
        const { currname } = req.body;
        const { acctypename } = req.body;
        const { clitypename } = req.body;
        const { password1 } = req.body;
        const { dob } = req.body;
        const { pincode } = req.body;
        const { statename } = req.body;
        const { cityname } = req.body;
        const { PlaceofBirth } = req.body;
        const { Nationality } = req.body;
        const { Citizenship } = req.body;
        const { Education_level } = req.body;
        const { Profession } = req.body;
        const { Employment_Status } = req.body;

        const { q1 } = req.body;
        const { q2 } = req.body;
        const { q3 } = req.body;
        const { q4 } = req.body;
        const { q5 } = req.body;
        const { q6 } = req.body;
        const { q7 } = req.body;
        const { q8 } = req.body;
        const { q9 } = req.body;
        const { q10 } = req.body;


        const { imgfname2 } = req.body;
        const { imgfname3 } = req.body;

        const { doctype2 } = req.body;
        const { doctype3 } = req.body;
        const { mobnocode } = req.body;
        var htype = "R";

        const insertdemo = await pool.query("insert into accountholders(email,fname,lname,mobileno,address,country,currency_choice,account_type,client_type,password,dob,holdertype,postalcode,statename,cityname,PlaceofBirth,Nationality,Citizenship,Education_level,Profession,Employment_Status,q1,q2,q3,q4,q5,q6,q7,q8,q9,imgname1,imgname2,imgname3,doctype1,doctype2,doctype3,countrycode) values($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26,$27,$28,$29,$30,$31,$32,$33,$34,$35,$36,$37)",
            [email, fname, lname, mobno, address, countryname,
                currname, acctypename, clitypename, password1,
                dob, htype, pincode, statename, cityname, PlaceofBirth,
                Nationality, Citizenship, Education_level, Profession,
                Employment_Status, q1, q2, q3, q4, q5, q6, q7, q8, q9,

                imgfname1, imgfname2, imgfname3,
                doctype1, doctype2, doctype3, mobnocode]);
        res.json(insertdemo);
    } catch (err) {
        console.log(err.message);
        res.json(err.message);
    }
});




app.post("/realupdateimg1", async (req, res) => {
    try {
        const { email } = req.body;
        const { imgdata1 } = req.body;
        const { imgfname1 } = req.body;
        const { doctype1 } = req.body;
        var st = "I";
        const insertdemo = await pool.query("insert into  kycdocument(imgname,doctype,imgdata,emailid,status) values($1,$2,$3,$4,$5)", [imgfname1, doctype1, imgdata1, email, st]);
        res.json(insertdemo);
    } catch (err) {
        console.log(err.message);
        res.json(err.message);
    }
});

app.post("/realupdateimg2", async (req, res) => {
    try {
        const { email } = req.body;
        const { imgdata2 } = req.body;
        const { imgfname2 } = req.body;
        const { doctype2 } = req.body;
        var st = "I";
        const insertdemo = await pool.query("insert into  kycdocument(imgname,doctype,imgdata,emailid,status) values($1,$2,$3,$4,$5)", [imgfname2, doctype2, imgdata2, email, st]);
        res.json(insertdemo);
    } catch (err) {
        console.log(err.message);
        res.json(err.message);
    }
});

app.post("/realupdateimg3", async (req, res) => {
    try {
        const { email } = req.body;
        const { imgdata3 } = req.body;
        const { imgfname3 } = req.body;
        const { doctype3 } = req.body;
        var st = "I";
        const insertdemo = await pool.query("insert into  kycdocument(imgname,doctype,imgdata,emailid,status) values($1,$2,$3,$4,$5)", [imgfname3, doctype3, imgdata3, email, st]);
        res.json(insertdemo);
    } catch (err) {
        console.log(err.message);
        res.json(err.message);
    }
});

app.get("/alldemo", async (req, res) => {
    try {


        const insertdemo = await pool.query("select * from  accountholders");
        res.json(insertdemo.rows);
    } catch (err) {
        console.log(err.message);
        res.json(err.message);
    }
});



const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});
